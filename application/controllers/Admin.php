<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller
{
    private $itemsPerPage = 20;

    function __construct()
    {
        parent::__construct();

        if ( !$this->user || !$this->user->Admin ) redirect('/');
    }

    function unapprovedUsers()
    {
        $data = array();
        $data['users'] = $this->user_model->getAllUnapprovedUsers();

        $this->load->view('admin_approve_users_view', $data);
    }

    function approveUser($userID)
    {
        $userID = (int)$userID;

        $user = $this->user_model->getUserByID($userID);

        if (!$user || $user->Approved) redirect(site_url().'/admin/approve-users');

        $this->user_model->approveUser($userID);
        $this->session->set_flashdata('msg', 'User approved');
        redirect(site_url().'/admin/approve-users');
    }
}