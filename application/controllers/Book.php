<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends MY_Controller
{
    private $itemsPerPage = 10;

    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
    }

    function index()
    {
        $this->booksList();
    }

    function booksList($page = 0)
    {
        $this->title = 'Books list | Library';

        $data = array();
        $data['page'] = $page;

        // BEGIN: filters
        $filters = array();

        // authorFilter
        $filters['authorFilter'] = '';
        if ( null !== $this->input->get('authorFilter') ) {
            $authorFilter = trim($this->input->get('authorFilter'));
            $filters['authorFilter'] = $authorFilter;
        }

        $data['authorFilter'] = $filters['authorFilter'];

        // yearFilter
        $filters['yearFilter'] = '';
        if ( null !== $this->input->get('yearFilter') ) {
            $yearFilter = trim($this->input->get('yearFilter'));
            $filters['yearFilter'] = $yearFilter;
        }

        $data['yearFilter'] = $filters['yearFilter'];
        // END

        $result = $this->book->getAllBooks($page, $this->itemsPerPage, $filters);

        $data['books'] = $result[1];

        // BEGIN: pagination
        $this->load->library('pagination');

        $config = array();
        $config['base_url']         = base_url().'book/list';
        $config['total_rows']       = $result[0];
        $config['per_page']         = $this->itemsPerPage;
        $config['page']             = $page;
        $config['num_links']        = 2;
        $config['use_page_numbers'] = TRUE;
        $config['first_link']       = '<<';
        $config['last_link']        = '>>';
        $config['first_tag_open'] = $config['last_tag_open'] = $config['next_tag_open'] = $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['cur_tag_open'] = '<li><a style="background-color: #286090; color: white">';
        $config['first_tag_close'] = $config['last_tag_close'] = $config['next_tag_close'] = $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($config);
        $data['pag'] = $this->pagination;
        // END

        $this->load->view('books_list_view', $data);
    }

    function newBook()
    {
        // permission - user must be logged in to add new books
        if (!$this->user || !$this->user->Approved) redirect(base_url());

        $this->title = 'New Book | LRS';

        $data = array();

        if ($_POST) {

            $this->form_validation->set_rules('Name', 'Book Name', 'trim|required');

            $book = $this->input->post();

            foreach ($book as $key => $val) {
                $book[$key] = trim($val);
            }

            if ($this->form_validation->run() == FALSE) {
                $data['book'] = (object)$book;
                $this->load->view('contractor_view', $data);
            } else {
                $bookID = $this->book->save($book);

                $this->session->set_flashdata('msg', 'Book saved');
                redirect(base_url() . 'book');
            }

        } else {
            $this->load->view('book_details_view', $data);
        }
    }

    function details($id = 0)
    {
        $this->title = 'Book | Library';

        $id = (int)$id;

        // permission - user must be logged in to add new books
        if (!$this->user || !$this->user->Approved) redirect(base_url());

        $data = array();
        $book = $this->book->getBookByID($id);

        if ($_POST) {
            $this->form_validation->set_rules('Name', 'Book Name', 'trim|required');

            $book = $this->input->post();

            foreach ($book as $key => $val) {
                $book[$key] = trim($val);
            }

            if ($this->form_validation->run() == FALSE) {
                $data['error'] = 'Please fill all required fields correctly';
            } else {
                $this->book->edit($id, $book);

                $data['msg'] = 'Book saved';

                $book = $this->book->getBookByID($id);
            }
        }

        $data['book'] = $book;

        $this->load->view('book_details_view', $data);
    }

    function delete($id)
    {
        $id = (int)$id;

        // permission
        if (!$this->user || !$this->user->Approved) redirect(base_url());

        $this->book->delete($id);
        $this->session->set_flashdata('msg', 'Book deleted');
        redirect(base_url() . 'book/list');
    }
}