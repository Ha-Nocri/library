<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
    }

    function index()
    {
        $this->login();
    }

    function login()
    {
        $this->title = 'Log in | Library';

        if ($_POST) {
            $username = trim($this->input->post('username'));
            $password = trim($this->input->post('password'));

            $user = $this->user_model->getUser($username, $password);

            if (!$user) {
                $this->session->set_flashdata('error', true);
            } else {
                $this->session->set_userdata('userID', $user->ID);

                redirect(base_url());
            }
        }

        $this->load->view('login_view');
    }

    function logout()
    {
        $this->session->unset_userdata('userID');
        redirect(base_url());
    }

    function register()
    {
        $this->title = 'Registration | Library';

        $data = array();

        if ($_POST) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[User.Username]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]');

            $data['Username'] = trim($this->input->post('username'));
            $data['Password'] = trim($this->input->post('password'));
            $data['confirmPassword'] = trim($this->input->post('confirmPassword'));

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', 'Please fill all required fields correctly');
            } else {
                unset($data['confirmPassword']);

                $userID = $this->user_model->save($data);

                //$this->session->set_userdata('userID', $userID);

                $this->load->view('thank_you_view', $data);
            }
        } else $this->load->view('register_view', $data);
    }

    function thankYou()
    {
        $this->load->view('thank_you_view');
    }

    function userExists()
    {
        if (!$this->input->is_ajax_request()) redirect(base_url());

        if (!$_POST) exit;

        $username = trim($this->input->post('username'));

        echo json_encode($this->user_model->userExists($username));
    }
}