<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $title = '';
    public $user = false;
    public $controller;
    public $method;
    public $noOfUnapprovedUsers;

    function __construct()
    {
        parent::__construct();

        $this->controller   = $this->router->fetch_class();
        $this->method       = $this->router->fetch_method();

        $this->load->model('user_model');
        $this->load->model('book_model', 'book');

        $this->noOfUnapprovedUsers = count($this->user_model->getAllUnapprovedUsers());

        $userID = $this->session->userdata('userID');
        if ($userID) $this->user = $this->user_model->getUserByID($userID);
    }
}