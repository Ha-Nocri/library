<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getAllBooks($page = 0, $itemsPerPage = 0, $filters = array())
    {
        $this->db->start_cache();
        $this->db->select();
        $this->db->where('Deleted', 0);
        if ($filters) $this->applyFilters($filters);
        $this->db->order_by('Name');
        $this->db->stop_cache();

        $result = array();
        $result[0] = $this->db->count_all_results('Book');

        if ($itemsPerPage > 0) {
            $totalPages = ceil($result[0] / $itemsPerPage);

            if ($page > $totalPages)
                $page = $totalPages;

            if ($page < 1)
                $page = 1;

            $begin = ($page - 1) * $itemsPerPage;
            $this->db->limit($itemsPerPage, $begin);
        }

        $result[1] = $this->db->get('Book')->result();
        $this->db->flush_cache();

        return $result;
    }

    private function applyFilters($filters)
    {
        if ($filters['authorFilter']) $this->db->like('Author', $filters['authorFilter']);
        if ($filters['yearFilter']) $this->db->where('Year', $filters['yearFilter']);
    }

    function getBookByID($id)
    {
        return $this->db->get_where('Book', array('ID' => $id))->row();
    }

    function save($data)
    {
        $this->db->insert('Book', $data);
    }

    function edit($id, $data)
    {
        $this->db->where('ID', $id)->update('Book', $data);
    }

    function delete($id)
    {
        $this->db->where('ID', $id)->set('Deleted', 1)->update('Book');
    }
}