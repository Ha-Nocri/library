<?php

class User_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getUser($username, $password)
    {
        $user = $this->db->get_where('User', array('Username' => $username, 'Password' => $password))->row();

        return $user ? $user : false;
    }

    function getUserByID($id)
    {
        return $this->db->get_where('User', array('ID' => $id))->row();
    }

    function isAdmin($userID)
    {
        $user = $this->getUserByID($userID);

        if ($user->ID == 1) return true;
        else return false;
    }

    function save($data)
    {
        $data['Approved'] = 0;
        $data['Admin']    = 0;

        $this->db->insert('User', $data);

        return $this->db->insert_id();
    }

    function userExists($username)
    {
        $user = $this->db->get_where('User', array('Username' => $username))->row();

        return $user ? true : false;
    }

    function getAllUnapprovedUsers()
    {
        return $this->db->get_where('User', array('Approved' => 0))->result();
    }

    function approveUser($userID)
    {
        $this->db->where('ID', $userID)->set('Approved', 1)->update('User');
    }

    function edit($id, $data)
    {
        $this->db->where('ID', $id)->update('User', $data);
    }
}