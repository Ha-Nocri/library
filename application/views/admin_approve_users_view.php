<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $this->title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php $this->load->view('header_view') ?>

<div class="well text-center main-container">
    <h3>Approve Users</h3>
</div>

<div class="container main-container" style="overflow: auto">

    <div class="row" style="font-weight: bold">
        <div class="col-sm-1">
            ID
        </div>
        <div class="col-sm-10">
            Username
        </div>
        <div class="col-sm-1">
            Action
        </div>
    </div>

    <br/>

    <?php if ($users): ?>
        <?php foreach ($users as $user): ?>
            <div class="row clickable-row">
                <div class="col-sm-1">
                    <?php echo $user->ID ?>
                </div>
                <div class="col-sm-10">
                    <?php echo $user->Username ?>
                </div>
                <div class="col-sm-1 no-side-padding">
                    <a href="<?php echo site_url()?>/admin/approve-user/<?php echo $user->ID ?>" title="Approve this User">approve</a>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="row clickable-row" style="pointer-events: none">
            <br/>
            <div class="col-sm-12 text-center">No results</div>
        </div>
    <?php endif; ?>

</div>

<?php $this->load->view('js_css_view') ?>
<?php $this->load->view('modals_view') ?>

<script type="text/javascript">
    $(function() {
        <?php if ($this->session->flashdata('msg')): ?>
        showNotification('<?php echo $this->session->flashdata('msg') ?>');
        <?php endif; ?>
    });
</script>

</body>
</html>