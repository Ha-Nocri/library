<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $this->title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php $this->load->view('header_view') ?>

<div class="well text-center main-container">
    <h3><?php echo isset($book->Name) ? $book->Name : 'New Book' ?></h3>
</div>

<div class="container main-container">

    <form method="post" onsubmit="return validation()">

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name"><span class="alert-text">*</span> Book Name:</label>
                    <input id="Name" name="Name" class="form-control important" maxlength="255" value="<?php echo isset($book->Name) ? $book->Name : '' ?>" />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="Author"><span class="alert-text">*</span> Author:</label>
                    <input id="Author" name="Author" class="form-control important" maxlength="255" value="<?php echo isset($book->Author) ? $book->Author : '' ?>" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="Year"><span class="alert-text">*</span> Publish Year:</label>
                    <input id="Year" name="Year" class="form-control integer-only important" maxlength="4" value="<?php echo isset($book->Year) ? $book->Year : '' ?>" />
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="Language"><span class="alert-text">*</span> Language:</label>
                    <input  id="Language" name="Language" class="form-control important" maxlength="255" value="<?php echo isset($book->Language) ? $book->Language : '' ?>" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="OriginalLanguage"><span class="alert-text">*</span> Original Language:</label>
                    <input id="OriginalLanguage" name="OriginalLanguage" class="form-control important" maxlength="255" value="<?php echo isset($book->OriginalLanguage) ? $book->OriginalLanguage : '' ?>" />
                </div>
            </div>
            <div class="col-sm-6"></div>
        </div>

        <hr>

        <div class="row" style="margin-bottom: 20px">
            <div class="col-sm-10" style="color: darkgray"><span class="alert-text">*</span> Required Fields</div>
            <div class="col-sm-2">
                <input type="submit" class="btn btn-primary float-right" style="width:100%" value="Submit" />
            </div>
        </div>
    </form>
</div>

<?php $this->load->view('js_css_view') ?>
<?php $this->load->view('modals_view') ?>

<script>
    $(function() {
        <?php if (isset($error) && $error): ?>
        showAlert('<?php echo $error ?>');
        <?php endif; ?>
        <?php if (isset($msg) && $msg): ?>
        showNotification('<?php echo $msg ?>');
        <?php endif; ?>
    });

    function validation()
    {
        clearValidationErrors();
        var valid = true;

        $('.important').each(function() {
            var val = $(this).val().trim();
            if (val === '') {
                valid = false;
                $(this).addClass('validation-error');
            }
        });

        return valid;
    }

    function clearValidationErrors()
    {
        $('.important').removeClass('validation-error');
    }
</script>
</body>