<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $this->title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php $this->load->view('header_view') ?>

<div class="well text-center main-container">
    <h3>Books</h3>
</div>

<div class="container main-container" style="overflow: auto">

    <!-- toolbar -->
    <div class="row">
        <div class="col-sm-5">
            <?php if ($pag->create_links()): ?>
                <ul class="pagination" style="margin-top: 0">
                    <?php echo $pag->create_links(); ?>
                </ul>
            <?php endif; ?>
        </div>

        <div class="col-sm-1"></div>

        <div class="col-sm-3">
            <div class="input-group">
                <input id="authorFilter" name="authorFilter" class="form-control" style="font-size: 12px;" placeholder="Search by Author" value="<?php echo $authorFilter ?>">
            </div>
        </div>

        <div class="col-sm-3">
            <div class="input-group">
                <input id="yearFilter" name="yearFilter" class="form-control" style="font-size: 12px;" placeholder="Search by Publish Year" value="<?php echo $yearFilter ?>">
                <div class="input-group-btn">
                    <button id="submit" class="btn btn-default" onclick="submitFilters()">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </div>
            </div>
        </div>
    </div><!-- toolbar -->

    <hr>

    <div class="row" style="font-weight: bold">
        <div class="col-sm-1">
            ID
        </div>
        <div class="col-sm-2">
            Book Name
        </div>
        <div class="col-sm-2">
            Author
        </div>
        <div class="col-sm-2">
            Publish Year
        </div>
        <div class="col-sm-2">
            Language
        </div>
        <div class="col-sm-2">
            Original Language
        </div>
        <?php if ($this->user && $this->user->Approved): ?>
        <div class="col-sm-1">
            Action
        </div>
        <?php endif; ?>
    </div>

    <br/>

    <?php if ($books): ?>
        <?php foreach ($books as $book): ?>
        <div class="row clickable-row">
            <div class="col-sm-1">
                <?php echo $book->ID ?>
            </div>
            <div class="col-sm-2">
                <?php echo $book->Name ?>
            </div>
            <div class="col-sm-2">
                <?php echo $book->Author ?>
            </div>
            <div class="col-sm-2">
                <?php echo $book->Year ?>
            </div>
            <div class="col-sm-2">
                <?php echo $book->Language ?>
            </div>
            <div class="col-sm-2">
                <?php echo $book->OriginalLanguage ?>
            </div>
            <?php if ($this->user && $this->user->Approved): ?>
            <div class="col-sm-1" style="padding-right: 0">
                <a href="<?php echo base_url()?>book/details/<?php echo $book->ID ?>" title="Edit this Book">edit</a>
                |
                <a href="<?php echo base_url()?>book/delete/<?php echo $book->ID ?>" title="Delete this Book">delete</a>
            </div>
            <?php endif; ?>
        </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="row clickable-row" style="pointer-events: none">
            <br/>
            <div class="col-sm-12 text-center">No results</div>
        </div>
    <?php endif; ?>

</div>

<?php $this->load->view('js_css_view') ?>
<?php $this->load->view('modals_view') ?>

<script type="text/javascript">
    $(function() {
        var yo = 'yooooo';
        <?php if ($this->session->flashdata('msg')): ?>
        showNotification('<?php echo $this->session->flashdata('msg') ?>');
        <?php endif; ?>
    });

    function submitFilters()
    {
        var authorFilter = $('#authorFilter').val().trim();
        var yearFilter = $('#yearFilter').val().trim();

        var get = '';
        if (authorFilter && yearFilter)
            get = '?authorFilter=' + authorFilter + '&yearFilter=' + yearFilter;
        else if (authorFilter)
            get = '?authorFilter=' + authorFilter;
        else if (yearFilter)
            get = '?yearFilter=' + yearFilter;

        window.location = '<?php echo base_url() ?>' + 'book/list/' + '<?php echo $page ?>' + get;
    }
</script>
</body>
</html>