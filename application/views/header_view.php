<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid main-container" style="padding: 0 10px 0 10px;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>">Library</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php if ($this->user && $this->user->Approved): ?>
            <ul class="nav navbar-nav">

                <li class="<?php echo $this->controller == 'contractor' ? 'active' : '' ?>"><a href="<?php echo base_url().'book/new/' ?>">Add new Book</a></li>

            </ul>
            <?php endif; ?>

            <ul class="nav navbar-nav navbar-right">

                <?php if ($this->user): ?>
                    <?php if ( $this->user->Approved && $this->user->Admin ): ?>
                        <li class="dropdown <?php echo $this->controller == 'admin' ? 'active' : '' ?>">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <span class="glyphicon glyphicon-cog"><?php if ($this->noOfUnapprovedUsers > 0): ?><sup style="background: darkred; color: white; padding: 0 2px 2px 2px"><strong><?php echo $this->noOfUnapprovedUsers ?></strong></sup><?php endif; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo base_url() ?>admin/approve-users">
                                        Approve Users
                                        <?php if ($this->noOfUnapprovedUsers > 0): ?>
                                            <sup style="background: darkred; color: white; padding: 0 2px 2px 2px"><strong><?php echo $this->noOfUnapprovedUsers ?></strong></sup>
                                        <?php endif; ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <li><a href="<?php echo base_url() ?>user/logout"><span class="glyphicon glyphicon-log-out"></span> Logout (<?php echo $this->user->Username ?>)</a></li>
                <?php else: ?>
                    <li><a href="<?php echo base_url() ?>user/register"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="<?php echo base_url() ?>user/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                <?php endif; ?>

            </ul>
        </div>
    </div>
</nav>