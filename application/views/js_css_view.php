<link rel="stylesheet" href="<?php echo base_url() ?>bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css/main.css">
<link rel="stylesheet" href="<?php echo base_url() ?>js/jquery-ui/jquery-ui.css">
<script src="<?php echo base_url() ?>js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url() ?>js/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url() ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>js/events/global.js"></script>
<script src="<?php echo base_url() ?>js/functions/global.js"></script>