<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $this->title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php $this->load->view('header_view') ?>

<div class="well text-center main-container">
    <h3>Log In</h3>
</div>

<div class="container main-container">

    <br/><br/><br/>
    <form method="post">

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input id="username" name="username" class="form-control" maxlength="255" placeholder="User Name" title="User Name" />
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <br/>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" id="password" name="password" class="form-control" maxlength="255" placeholder="Password" title="Password" />
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <br/>

        <div class="row">
            <div class="col-sm-5"></div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" value="Submit" />
                </div>
            </div>
        </div>

    </form>

</div>

<?php $this->load->view('js_css_view') ?>
<?php $this->load->view('modals_view') ?>

<script type="text/javascript">
    $(function() {
        <?php if ($this->session->flashdata('error')): ?>
        showAlert('Wrong User Name and/or Password');
        <?php endif; ?>
    });
</script>
</body>