<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $this->title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php $this->load->view('header_view') ?>

<div class="well text-center main-container">
    <h3>Registration</h3>
</div>

<div class="container main-container">

    <br/>

    <form method="post" onsubmit="return validation()">

        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="username">User Name:</label>
                    <input id="username" name="username" class="form-control important" maxlength="255" />
                </div>
            </div>
            <div id="userExists" class="col-sm-3 alert-text display-none">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <div style="padding-top: 10px">User Name already taken</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" id="password" name="password" class="form-control important" maxlength="255" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="confirmPassword">Confirm Password:</label>
                    <input type="password" id="confirmPassword" name="confirmPassword" class="form-control important" maxlength="255" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-5"></div>
            <div class="col-sm-2">
                <div class="form-group">
                    <input id="submit" type="submit" class="btn btn-primary btn-block" value="Submit" />
                </div>
            </div>
        </div>

    </form>
</div>

<?php $this->load->view('js_css_view') ?>
<?php $this->load->view('modals_view') ?>

<script type="text/javascript">
    var userExists = false;

    $(function() {
        <?php if (validation_errors()): ?>
        showAlert("Please fill all fields correctly");
        <?php endif; ?>

        $('.important').on('change', function() {
            $(this).removeClass('validation-error');

            var val = $(this).val().trim();

            if (val === '') $(this).addClass('validation-error');
            else {
                if ($(this).attr('id') === 'username') {
                    $.ajax({
                        url: "<?php echo base_url() ?>user/userExists",
                        data: {username: val},
                        method: 'post',
                        async: false
                    }).done(function (data) {
                        if (data === 'true') {
                            userExists = true;
                            $('#userExists').removeClass('display-none');
                            $('#username').addClass('validation-error');
                        }
                        if (data === 'false') {
                            userExists = false;
                            $('#userExists').addClass('display-none');
                        }
                    });
                }

                var password = $('#password').val().trim();
                var confirmPassword = $('#confirmPassword').val().trim();
                if ($(this).attr('id') === 'password' && confirmPassword !== '' && val !== confirmPassword)
                    $('#confirmPassword').addClass('validation-error');
                if ($(this).attr('id') === 'password' && confirmPassword !== '' && val === confirmPassword)
                    $('#confirmPassword').removeClass('validation-error');
                if ($(this).attr('id') === 'confirmPassword' && password !== '' && val !== password)
                    $(this).addClass('validation-error');
            }
        });
    });

    function validation()
    {
        var valid = true;

        $('.important').removeClass('validation-error').each(function() {
            var val = $(this).val().trim();

            if (val === '') {
                valid = false;
                $(this).addClass('validation-error');
            } else {
                if ($(this).attr('id') === 'username' && userExists) {
                    valid = false;
                    $(this).addClass('validation-error');
                }

                var password = $('#password').val().trim();
                if ($(this).attr('id') === 'confirmPassword' && password !== '' && val !== password) {
                    valid = false;
                    $(this).addClass('validation-error');
                }
            }
        });

        return valid;
    }
</script>
</body>