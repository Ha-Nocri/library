<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $this->title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php $this->load->view('header_view') ?>

<div class="container main-container">
    <h3>Thank you for registering</h3>
    <p>You have successfully finished your registration.</p>
    <p>You will be able to add/edit/delete Books as soon as our administrators check your account and approve it.</p>
    <p>Thank you for understanding.</p>
</div>

<?php $this->load->view('js_css_view') ?>

</body>