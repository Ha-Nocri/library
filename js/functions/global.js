function showAlert(message)
{
    $('#alertDialogMessage').html(message);
    $('#alertDialog').modal('show');
}

function showNotification(message)
{
    $('#notificationDialogMessage').html(message);
    $('#notificationDialog').modal('show');
}