-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2017 at 05:10 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_bin NOT NULL,
  `Author` varchar(255) COLLATE utf8_bin NOT NULL,
  `Year` int(11) NOT NULL,
  `Language` varchar(255) COLLATE utf8_bin NOT NULL,
  `OriginalLanguage` varchar(255) COLLATE utf8_bin NOT NULL,
  `Deleted` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`ID`, `Name`, `Author`, `Year`, `Language`, `OriginalLanguage`, `Deleted`) VALUES
(1, 'Majstor i Margarita', 'Mihail Bulgakov', 1972, 'Srpski', 'Engleski', b'0'),
(2, 'Predeo slikan čajem', 'Milorad Pavić', 1988, 'Srpski', 'Srpski', b'0'),
(3, 'testname', 'testautor', 1200, 'Albanian', 'Croatian', b'0'),
(4, '1', '1', 1, '1', '1', b'0'),
(5, '2', '2', 2, '2', '2', b'0'),
(6, '3', '3', 3, '3', '3', b'0'),
(7, '4', '4', 4, '4', '4', b'0'),
(8, '5', '5', 5, '5', '5', b'0'),
(9, '6', '6', 6, '6', '6', b'0'),
(10, '7', '7', 7, '7', '7', b'0'),
(11, '8', '8', 8, '8', '8', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `Approved` bit(1) NOT NULL,
  `Admin` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Username`, `Password`, `Approved`, `Admin`) VALUES
(1, 'admin', 'admin', b'1', b'1'),
(3, 'pedja', 'test1234', b'1', b'0'),
(4, 'test', 'test1234', b'0', b'0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Email` (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
